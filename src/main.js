// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'

import VueRouter from "vue-router";
import VueResource from "vue-resource";
import Survey from "./components/Survey";
import Admin from "./components/Admin";
import Success from "./components/Success.vue";

Vue.use(VueResource);
Vue.use(VueRouter);

const router = new VueRouter({
    mode: "history",
    base: __dirname,
    routes: [
        { path: "/", component: Survey },
        { path: "/admin", component: Admin },
        { path: "/success", component: Success }
    ]
});

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
    router,
    template: `
    <div id = "app">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <router-link to="/" class="navbar-brand">NPS Survey</router-link>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="nav navbar-nav">
        <li class="nav-item">
          <router-link to="/admin" class="nav-link">Admin Report</router-link>
        </li>
      </ul>
    </div>
  </nav>
      <router-view></router-view>
    </div>
  `
}).$mount("#app");
